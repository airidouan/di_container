package TestAutoRegistering1.V2;

import Annotations.InjectAttribute;
import Annotations.ToBeRegistered;
import TestAutoRegistering1.ImplementationG_v1;

@ToBeRegistered
public class ImplementationG_v2 {

    private String name = "version2";

    @InjectAttribute
    private ImplementationG_v1 implementationG_v1;

    public String getName() {
        return name;
    }

    public String TaskG() {
        return name + ": I have " + implementationG_v1.getName();
    }
}
