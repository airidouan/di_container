package TestAutoRegistering1;

import Annotations.InjectAttribute;
import Annotations.ToBeRegistered;
import TestAutoRegistering1.V2.ImplementationG_v2;

@ToBeRegistered
public class ImplementationG_v1 {

    private String name = "version1";

    @InjectAttribute
    private ImplementationG_v2 implementationG_v2;

    @InjectAttribute
    private ImplementationG_v3 implementationG_v3;

    public ImplementationG_v3 getImplementationG_v3() {
        return implementationG_v3;
    }

    public String getName() {
        return name;
    }

    public String TaskG() {
        return name + ": I have " + implementationG_v2.getName();
    }
}
