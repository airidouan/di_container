package Implementations;

import Abstractions.ClassB;

public class ImplementationB_v2 implements ClassB {

    private String name;

    public ImplementationB_v2()
    {
        name = "none";
    }

    public ImplementationB_v2(String name)
    {
        this.name = name;
    }

    @Override
    public String TaskB() {
        return "My name is " + name;
    }
}
