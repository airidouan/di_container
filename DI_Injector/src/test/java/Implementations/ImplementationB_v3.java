package Implementations;

import Abstractions.ClassB;
import Annotations.InjectConstructor;

public class ImplementationB_v3 implements ClassB {

    private String name;

    public ImplementationB_v3()
    {
        name = "none";
    }

    @InjectConstructor
    public ImplementationB_v3(String name)
    {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String TaskB() {
        return "My name is " + name;
    }
}
