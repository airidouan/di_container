package Implementations;

import Abstractions.ClassA;
import Annotations.InjectAttribute;

public class ImplementationE{

    @InjectAttribute
    private ClassA classA;

    @InjectAttribute
    private ImplementationF classF;

    public String TaskE() {
        return "The name of my classA is " + classA.getName();
    }

    public ClassA getClassA()
    {
        return classA;
    }

    public ImplementationF getClassF() { return classF;}
}
