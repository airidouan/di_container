package Implementations;

import Abstractions.ClassB;

public class ImplementationB_v1 implements ClassB {

    private String name;

    public ImplementationB_v1(String name)
    {
        this.name = name;
    }

    @Override
    public String TaskB() {
        return "My name is " + name;
    }
}
