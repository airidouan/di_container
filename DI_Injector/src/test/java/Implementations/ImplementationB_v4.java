package Implementations;

import Abstractions.ClassB;
import Annotations.InjectConstructor;

public class ImplementationB_v4 implements ClassB {

    private String name;

    @InjectConstructor
    public ImplementationB_v4(String name1, String name2)
    {
        name = name1.concat(",").concat(name2);
    }

    @InjectConstructor
    public ImplementationB_v4(String name)
    {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String TaskB() {
        return "My name is " + name;
    }
}
