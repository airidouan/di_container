package Implementations;

import Abstractions.ClassA;

public class ImplementationA implements ClassA {

    private String name= "Default name";

    @Override
    public String getName() {
        return name;
    }
}
