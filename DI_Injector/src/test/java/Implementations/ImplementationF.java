package Implementations;

import Abstractions.ClassA;
import Annotations.InjectAttribute;

public class ImplementationF {

    private ClassA classA;

    @InjectAttribute
    private ImplementationE classE;

    public String TaskF() {
        return "The name of my classA is " + classA.getName();
    }

    public ClassA getClassA()
    {
        return classA;
    }

    public ImplementationE getClassE() { return classE;}
}
