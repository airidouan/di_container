package TestAutoRegistering2;

import Annotations.ToBeRegistered;

@ToBeRegistered
public class ImplementationG_v4 {
    private String name = "version4";

    public String getName()
    {
        return name;
    }
}
