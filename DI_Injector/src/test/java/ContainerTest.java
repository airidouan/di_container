import Abstractions.ClassA;
import Abstractions.ClassB;
import Abstractions.ClassCD;
import Container.DI_Injector;
import Exceptions.MultipleDefinedAnnotatedConstructorsException;
import Exceptions.NoConstructorWasChosenException;
import TestAutoRegistering1.ImplementationG_v1;
import TestAutoRegistering1.V2.ImplementationG_v2;
import Implementations.*;
import TestAutoRegistering2.ImplementationG_v4;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ContainerTest {

    DI_Injector injector;

    @BeforeEach
    void createInjector() {
        injector = new DI_Injector();
    }

    @Test
    @DisplayName("Test simple injection using the default constructor of ImplementationA")
    void testInjectionUsingDefaultConstructor() throws Exception {
        injector.register(ImplementationA.class);

        ClassA newInstanceA = injector.newInstance(ImplementationA.class);

        assertTrue(newInstanceA instanceof ImplementationA);
        assertTrue(newInstanceA.getName() == "Default name");
    }

    @Test
    @DisplayName("Test injection of an abstraction using the default constructor of ImplementationA")
    void testInjectionOfAbstractionUsingDefaultConstructor() throws Exception {
        injector.register(ImplementationA.class);

        injector.bind(ClassA.class, ImplementationA.class);
        ClassA newInstanceA = injector.newInstance(ClassA.class);

        assertTrue(newInstanceA instanceof ImplementationA);
        assertTrue(newInstanceA.getName().equals("Default name"));
    }

    @Test
    @DisplayName("Test injection with a non-default and non-annotated constructor only, it should throw an exception since we do not have the default constructor anymore")
    void testInjectionWithNonDefaultNonAnnotatedConstructorOnly() throws Exception {
        injector.register(ImplementationB_v1.class);

        Exception exception = assertThrows(NoConstructorWasChosenException.class, () -> {
            ClassB newInstanceB_v1 = injector.newInstance(ImplementationB_v1.class);
        });

        String messageException = "You should either redefine the default constructor or annotate your chosen constructor with @InjectConstructor for the class: " + ImplementationB_v1.class.getName();
        assertEquals(messageException, exception.getMessage());
    }

    @Test
    @DisplayName("Test injection with a default and a non-default constructor but none of them is annotated, the default constructor will be used to inject")
    void testInjectionWithNoChosenConstructor() throws Exception {
        injector.register(ImplementationB_v2.class);

        ClassB newInstanceB_v2 = injector.newInstance(ImplementationB_v2.class);

        assertEquals("My name is none", newInstanceB_v2.TaskB());
    }

    @Test
    @DisplayName("Test injection with a default constructor and a non-default but annotated one, the non-default constructor will be used to inject")
    void testInjectionWithChosenConstructor() throws Exception {
        injector.register(ImplementationB_v3.class);

        ImplementationB_v3 newInstanceB_v3 = injector.newInstance(ImplementationB_v3.class);

        assertEquals("My name is ", newInstanceB_v3.TaskB());
        assertEquals("", newInstanceB_v3.getName());
    }

    @Test
    @DisplayName("Test injection with a two annotated constructors, throw an exception since one only constructor can be used")
    void testInjectionWithTwoChosenConstructor() throws Exception {
        injector.register(ImplementationB_v4.class);

        Exception exception = assertThrows(MultipleDefinedAnnotatedConstructorsException.class, () -> {
            ImplementationB_v4 newInstanceB_v4 = injector.newInstance(ImplementationB_v4.class);
        });

        String messageException = "Make sure to specify one only annotated constructor for the class " + ImplementationB_v4.class.getName() +"! (" + 2 + " annotated constructors were found)";
        assertEquals(messageException, exception.getMessage());
    }

    @Test
    @DisplayName("Test injection before and after changing the binding between abstraction and implementation")
    void testInjectionWithBeforeAndAfterChangingTheImplementation() throws Exception {
        injector.register(ClassCD.class);

        injector.bind(ClassCD.class, ImplementationC.class);
        ClassCD implementationCD = injector.newInstance(ClassCD.class);

        assertTrue(implementationCD instanceof ImplementationC);
        assertTrue(implementationCD.TaskCD().equals("TaskCD from ImplementationC"));

        injector.bind(ClassCD.class, ImplementationD.class);
        implementationCD = injector.newInstance(ClassCD.class);
        assertTrue(implementationCD instanceof ImplementationD);
        assertTrue(implementationCD.TaskCD().equals("TaskCD from ImplementationD"));
    }

    @Test
    @DisplayName("Test injection for a class with a simple dependency (one way)")
    void testInjectionWithOneDependency() throws Exception {
        injector.register(ImplementationA.class);
        injector.register(ImplementationE.class);

        ImplementationE implementationE = injector.newInstance(ImplementationE.class);

        assertTrue(implementationE.getClassA() instanceof ClassA);
        assertTrue(implementationE.getClassA().getName().equals("Default name"));
        assertTrue(implementationE.TaskE().equals("The name of my classA is Default name"));
    }

    @Test
    @DisplayName("Test injection for a class with a simple dependency that is not registered")
    void testInjectionWithOneDependencyWithoutRegisteringThisDependency() throws Exception {
        injector.register(ImplementationE.class);

        ImplementationE implementationE = injector.newInstance(ImplementationE.class);

        assertTrue(implementationE.getClassA() == null);
    }

    @Test
    @DisplayName("Test injection for a class with a simple dependency that is not annotated")
    void testInjectionWithOneDependencyThatIsNotAnnotated() throws Exception {
        injector.register(ImplementationF.class);

        ImplementationF implementationF = injector.newInstance(ImplementationF.class);

        assertTrue(implementationF.getClassA() == null);
    }

    @Test
    @DisplayName("Test injection for two way dependency")
    void testInjectionWithTwoWayDependency() throws Exception {
        injector.register(ImplementationA.class);
        injector.register(ImplementationE.class);
        injector.register(ImplementationF.class);

        ImplementationE implementationE = injector.newInstance(ImplementationE.class);
        ImplementationF implementationF = injector.newInstance(ImplementationF.class);

        assertTrue(implementationE.getClassA() instanceof ClassA);
        assertTrue(implementationE.getClassF() instanceof ImplementationF);

        assertTrue(implementationE.getClassA().getName().equals("Default name"));
        assertTrue(implementationE.TaskE().equals("The name of my classA is Default name"));
        assertFalse(implementationE.getClassF() == null);

        assertTrue(implementationF.getClassE() instanceof ImplementationE);
        assertTrue(implementationF.getClassE() != null);
        assertTrue(implementationF.getClassA() == null);


    }

    @Test
    @DisplayName("Test injection with auto registered classes")
    void testInjectionWithAutoRegisteredClasses() throws Exception {
        int number = injector.registerAnnotatedClasses("TestAutoRegistering1");

        ImplementationG_v1 version1 = injector.newInstance(ImplementationG_v1.class);
        ImplementationG_v2 version2 = injector.newInstance(ImplementationG_v2.class);

        // Since we have only to annotated classes to be registered
        assertTrue(number == 2);

        // Since both classes has been registered
        assertFalse(version1 == null);
        assertFalse(version2 == null);

        // Dependencies should be injected in both object
        assertEquals("version1: I have version2", version1.TaskG());
        assertEquals("version2: I have version1", version2.TaskG());

        // Since the ImplementationA has not been registered (neither automatically with the annotation not with the method register)
        assertEquals(null, version1.getImplementationG_v3());
    }

    @Test
    @DisplayName("Test injection with auto registered classes that are defined in two different packages in the same level (none of them is a parent package for the other)")
    void testInjectionWithAutoRegisteredClassesDividedITwoPackages() throws Exception {
        int number1 = injector.registerAnnotatedClasses("TestAutoRegistering1");
        int number2 = injector.registerAnnotatedClasses("TestAutoRegistering2");

        ImplementationG_v1 version1 = injector.newInstance(ImplementationG_v1.class);
        ImplementationG_v2 version2 = injector.newInstance(ImplementationG_v2.class);
        ImplementationG_v4 version4 = injector.newInstance(ImplementationG_v4.class);

        // Since we have only to annotated classes to be registered
        assertTrue(number1 == 2);
        assertTrue(number2 == 1);

        // Since both classes has been registered
        assertFalse(version1 == null);
        assertFalse(version2 == null);
        assertFalse(version4 == null);

        // Dependencies should be injected for in both object
        assertEquals("version1: I have version2", version1.TaskG());
        assertEquals("version2: I have version1", version2.TaskG());


        assertEquals("version4", version4.getName());

        // Since the ImplementationA has not been registered (neither automatically with the annotation not with the method register)
        assertEquals(null, version1.getImplementationG_v3());
    }
}