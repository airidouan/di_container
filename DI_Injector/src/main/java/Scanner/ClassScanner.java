package Scanner;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class ClassScanner {

    // Get all the classes defined in the given package
    public static Set<Class<?>> getPackageClasses(String packageName) throws Exception {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        String path = packageName.replace('.', '/');

        // Get all directories in the prepared path
        List<File> directories = findDirectories(classLoader, path);

        Set<Class<?>> classes = new HashSet<>();
        for (File directory : directories) {
            // for each directory, get all its defined classes
            classes.addAll(findClasses(directory, packageName));
        }
        return classes;
    }

    private static List<File> findDirectories(ClassLoader classLoader, String path) throws IOException {
        Enumeration<URL> resources = classLoader.getResources(path);

        List<File> directories = new ArrayList<>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            directories.add(new File(resource.getFile()));
        }
        return  directories;
    }

    private static List<Class<?>> findClasses(File directory, String packageName) throws Exception {
        List<Class<?>> classes = new ArrayList<>();

        if (!directory.exists()) {
            // if the directory does nor exist, return an empty list
            return classes;
        }

        // Get all files in the directory
        File[] files = directory.listFiles();
        for (File file : files) {
            // if the directory contains another directory
            if (file.isDirectory()) {
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            }
            // otherwise, check if the file's extension is .class
            else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

}