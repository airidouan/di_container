package Exceptions;

public class NoConstructorWasChosenException extends Exception {
    private String message;

    public NoConstructorWasChosenException(String className)
    {
        message = "You should either redefine the default constructor or annotate your chosen constructor with @InjectConstructor for the class: " + className;
    }

    public String getMessage()
    {
        return message;
    }
}
