package Exceptions;

public class NoImplementationWasGivenException extends Exception {
    private String message;
    public NoImplementationWasGivenException(String abstraction)
    {
        message = "No implementation was given for " + abstraction;
    }

    public String getMessage()
    {
        return message;
    }
}
