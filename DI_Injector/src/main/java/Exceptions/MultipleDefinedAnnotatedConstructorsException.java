package Exceptions;

public class MultipleDefinedAnnotatedConstructorsException extends Exception {

    String message;


    public MultipleDefinedAnnotatedConstructorsException(int number, String className)
    {
        this.message = "Make sure to specify one only annotated constructor for the class " + className +"! (" + number + " annotated constructors were found)";
    }

    public String getMessage()
    {
        return message;
    }
}
