package Container;

import Annotations.InjectConstructor;
import Annotations.InjectAttribute;
import Annotations.ToBeRegistered;
import Exceptions.MultipleDefinedAnnotatedConstructorsException;
import Exceptions.NoConstructorWasChosenException;
import Exceptions.NoImplementationWasGivenException;
import Scanner.ClassScanner;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

public class DI_Injector {

    private final HashMap<Class<?>, Class<?>> implementations = new HashMap<>();
    private final HashMap<Class<?>, Object> instances = new HashMap<>();

    // Register all the classes with a null value or change them if they already exist
    public void register(Class<?> newClass) throws Exception {
        if (instances.containsKey(newClass))
            this.instances.replace(newClass, null);
        else
            this.instances.put(newClass, null);
    }

    // Change the implementation if the abstraction is already exist, otherwise add it to the HashMap
    public void bind(Class<?> abstraction, Class<?> implementation) {
        if (implementations.keySet().contains(abstraction))
            implementations.replace(abstraction, implementation);
        else
            implementations.put(abstraction, implementation);
    }

    // Get the latest chosen implementation
    public <T> Class<T> getImplementation(Class<T> abstraction) {
        if (implementations.keySet().contains(abstraction))
            return (Class<T>) implementations.get(abstraction);
        return null;
    }

    // Create the instance and save it + return it
    public <T> T newInstance(Class<T> _class) throws Exception {

        String className = _class.getName();
        // If the developer want an instance of an interface, we took the chosen implementation
        if (_class.isInterface())
        {
            _class = getImplementation(_class);
            register(_class);
            // If no implementation was given, throw NoImplementationWasGiven's exception
            if (_class == null)
                throw new NoImplementationWasGivenException(className);
        }

        // Check if the instance is already created
        T newInstance = (T)this.instances.get(_class);
        if(newInstance != null)
            return newInstance;

        // Get the base instance (with all dependencies are equal to null)
        newInstance = GetBasedInstance(_class);
        this.instances.replace(_class, newInstance);

        // Get all declared fields
        for (Field field : newInstance.getClass().getDeclaredFields()) {
            // If the field is not annotated with @InjectAttribute
            if (!field.isAnnotationPresent(InjectAttribute.class)) {
                continue;
            }

            Class<?> fieldType = field.getType();
            field.setAccessible(true);
            for (Class instance : this.instances.keySet()) {
                // Get the instance from the hashtable
                if (fieldType.isAssignableFrom(instance)) {
                    field.set(newInstance, newInstance(instance));
                    break;
                }
            }
        }
        return (T)this.instances.get(_class);
    }

    // Create an instance with default value for primitive types (not all of them) and null for others
    private <T> T GetBasedInstance(Class<T> aClass) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoConstructorWasChosenException, MultipleDefinedAnnotatedConstructorsException {
        // First, we get all the declared constructors
        Constructor<?>[] constructors = aClass.getDeclaredConstructors();
        Constructor constructor = null;
        int numberOfAnnotatedConstructors = 0;

        // We took the first constructor annotated with InjectConstructor
        for (Constructor construct : constructors) {
            if(construct.isAnnotationPresent(InjectConstructor.class))
            {
                constructor = construct;
                numberOfAnnotatedConstructors++;
            }
        }

        if (numberOfAnnotatedConstructors > 1)
        {
            throw new MultipleDefinedAnnotatedConstructorsException(numberOfAnnotatedConstructors, aClass.getName());
        }

        // If no constructor was declared or if none of them is annotated with InjectConstructor
        if (constructors.length == 0 || constructor == null)
        {
            try {
                // Get the default constructor
                constructor = aClass.getConstructor();
                constructor.setAccessible(true);
                return (T) constructor.newInstance();
            } catch (NoSuchMethodException exception)
            {
                // If the developer declare a non-default constructor the default one is no longer exists
                throw new NoConstructorWasChosenException(aClass.getName());
            }
        }

        // If the developer annotated a constructor with @InjectionConstructor
        // Get all parameters
        Parameter[] parameters = constructor.getParameters();
        Object[] args = new Object[parameters.length];

        // For each parameter, put a default value if it is a primitive type, otherwise null
        for (int i = 0; i < parameters.length; i++) {
            Parameter parameter = parameters[i];
            Class<?> type = parameter.getType();
            args[i] = getValueOfClass(type);
        }
        constructor.setAccessible(true);
        return (T) constructor.newInstance(args);
    }

    // Get the "default" value of a given type
    private Object getValueOfClass(Class<?> type) {
        if (Number.class.isAssignableFrom(type)) {
            if (Double.class.isAssignableFrom(type)
                    || Float.class.isAssignableFrom(type)) {
                return 0.0;
            } else {
                return 0;
            }
        } else if (Boolean.class.isAssignableFrom(type)) {
            return false;
        } else if (Date.class.isAssignableFrom(type)) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            return dtf.format(now);
        } else if (String.class.isAssignableFrom(type)) {
            return "";
        } else {
            // For dependencies
            return null;
        }
    }

    public int registerAnnotatedClasses(String packageName) throws Exception {
        int numberOfScannedClasses = 0;
        Set<Class<?>> allClassesInPackage = ClassScanner.getPackageClasses(packageName);
        for(Class<?> aClass : allClassesInPackage){
            if(aClass.isAnnotationPresent(ToBeRegistered.class)){
                this.instances.put(aClass, null);
                numberOfScannedClasses++;
            }
        }
        return numberOfScannedClasses;
    }
}
