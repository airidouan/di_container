# DI_Injector



## Overview
The aim of this project is to develop a simple yet functional Dependency Injector container that provide different methods which allow us to inject needed dependencies in the needed classes.\
In this document, we are going to explain how to use the container properly.

## Registering dependencies
In order to register the dependencies, the container provide 2 different ways:
* Register each dependency via the method:
```
 public void register(Class<?> newClass);
```
* Annotate the dependencies with the annotation bellow
```
  @ToBeRegistered
```
In order to find the annotated dependencies, you should call the method bellow. This method take as a parameter the name of the package where your dependencies are defined and return the number of dependencies that were registered in the container.
```
  public int registerAnnotatedClasses(String packageName);
```
Note that this method scan also all the packages that are inside the given one and it can be called as many as packages you want to scan.

## Abstraction & implementation

To bind an abstraction to a specific implementation, you may use the method:
```
  public void bind(Class<?> abstraction, Class<?> implementation);
```
If you want to change the chosen implementation, all what you need to do is to call the same method with the new implementation.

## Instance
If you intend to get an instance of a class that you already registered ( either via register method or ToBeRegistered annotation), all that you need to do is call the method:
```
   public <T> T newInstance(Class<T> _class);
```
This way, you will get an instance with all needed dependencies (that were all registered previously).
## Annotations
Three annotations can be used with this container:
* **@InjectAttibute** \
Used to specify which attribute should be injected inside the class.
* **@InjectConstructor** \
It serves to choose which of the defined constructors will be used to inject dependencies.\
The default constructor will be chosen if you did not define any other constructors or if you did not specify which one between them you want to use.\
Please note that if you define some constructors yet you did not annotate any of them to be used in injection, you should provide the class with the default constructor too, otherwise an exception will be thrown.
Please note also, that only one defined constructor can be annotated, otherwise an exception will be thrown.
* **@ToBeInjected** \
To specify the dependencies that you want to be scanned automatically (do not forget to call the method registerAnnotatedClasses with all the packages that contains these dependencies).


## Exceptions

In order to control the errors that may stop the container from working properly, 3 customized exceptions were used:
* **MultipleDefinedAnnotatedConstructorsException** \
Thrown if more than one constructor has been annotated with **@InjectConstructor**.
* **NoConstructorWasChosenException** \
Thrown if the default constructor is not provided and none of the provided ones was annotated.
* **NoImplementationWasGivenException** \
Thrown if you attempt to instantiate an abstraction without binding it first to an implementation.




## Unit Tests

The unit tests provided cover the following cases:

- [ ] Test simple injection using default constructor;
- [ ] Test injection of an abstraction using the default constructor;
- [ ] Test injection with a non-default and non-annotated constructor;
- [ ] Test injection with a default and a non-default constructor but none of them is annotated;
- [ ] Test injection with a default constructor and a non-default but annotated one;
- [ ] Test injection with a two annotated constructors;
- [ ] Test injection before and after changing the implementation of an abstraction;
- [ ] Test injection for a class with a simple dependency (one way);
- [ ] Test injection for a class with a simple dependency that is not registered;
- [ ] Test injection for a non-annotated class with a simple dependency;
- [ ] Test injection for two way dependency;
- [ ] Test injection with auto registered classes;
- [ ] Test injection with auto registered classes that are defined in two different packages in the same level.



***

# References
In order to achieve the objective of this TP, and in addition to the teacher's lecture, many websites were used:
* Basics:
  * [dev.to](https://dev.to/martinhaeusler/understanding-dependency-injection-by-writing-a-di-container-from-scratch-part-1-1hdf)
  * [deinum.biz](https://deinum.biz/2020-07-28-Dependency-Injection/#listing9)
  * [tabnine.com](https://www.tabnine.com/code/java/methods/java.lang.reflect.Constructor/getParameters)
* Reflection:
  * [baeldung.com](https://www.baeldung.com/java-reflection)
